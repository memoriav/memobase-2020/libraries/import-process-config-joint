import Dependencies._

ThisBuild / scalaVersion := "2.12.11"
ThisBuild / organization := "ch.memobase"
ThisBuild / organizationName := "Memoriav"
ThisBuild / startYear := Some(2020)
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") {
    Some(tag)
  } else {
    None
  }
}

lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "Import Process Config Joint",
    assemblyJarName in assembly := "app.jar",
    test in assembly := {},
    git.baseVersion := "0.1.0",
    git.useGitDescribe := true,
    bintrayOrganization := Some("memoriav"),
    bintrayRepository := "memobase",
    bintrayVersionAttributes := Map(),
    bintrayVcsUrl := Some(
      "https://gitlab.switch.ch/memoriav/memobase-2020/libraries/import-process-config-joint"
    ),
    libraryDependencies ++= Seq(
      kafkaStreams % "provided",
      log4jApi,
      log4jScala,
      kafkaStreamsTestUtils % Test,
      scalaTest % Test,
      uPickle % Test
    ),
    licenses += ("Apache-2.0", new URL(
      "https://www.apache.org/licenses/LICENSE-2.0.txt"
    ))
  )
