/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase

import java.util.Properties

import org.apache.kafka.common.header.internals.{RecordHeader, RecordHeaders}
import org.apache.kafka.common.serialization.Serdes.StringSerde
import org.apache.kafka.common.serialization._
import org.apache.kafka.streams.test.{ConsumerRecordFactory, OutputVerifier}
import org.apache.kafka.streams.{StreamsBuilder, StreamsConfig, TopologyTestDriver}
import org.scalatest.funsuite.AnyFunSuite
import upickle.default.{macroRW, read, write, ReadWriter => RW}

import scala.collection.JavaConverters._

class ConfigJoinerTest extends AnyFunSuite {

  import org.apache.kafka.streams.scala.ImplicitConversions.wrapKStream

  ignore("") {
    val streamsAppConfigs = new Properties()
    streamsAppConfigs.put(StreamsConfig.APPLICATION_ID_CONFIG, "test")
    streamsAppConfigs.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234")
    streamsAppConfigs.put(
      StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,
      Serdes.String().getClass.getName
    )
    streamsAppConfigs.put(
      StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,
      Serdes.String().getClass.getName
    )

    val builder = new StreamsBuilder

    val dataInputTopic = builder.stream[String, String]("data-input-topic")
    val configInputTopic = builder.stream[String, String]("config-input-topic")

    // Unfortunately, because of a serialization issue (see https://github.com/jpzk/mockedstreams/issues/25)
    // we need to read in the config as String and convert it subsequentally to a byte array.
    // Furthermore, the wrapKStream and the inner method respectively are needed to convert between the java KStream
    // and the Scala KStream object
    // Both shouldn't be an issue when using the library in a pure Scala or Kotlin/Java project
    val transformedConfig =
      wrapKStream(configInputTopic).mapValues(v => v.getBytes)

    val configJoiner =
      ConfigJoiner[String, TestConfig](
        "mapping",
        new StringSerde,
        new TestConfigSerDe,
        TestConfigHelper.parse
      )

    val joinedStream =
      configJoiner.join(dataInputTopic, transformedConfig.inner)
    wrapKStream(joinedStream)
      .mapValues(v => s"${v._1}#${v._2.akey}")
      .inner
      .to("output-topic")

    val topology = builder.build()

    val testDriver = new TopologyTestDriver(topology, streamsAppConfigs)

    val dataFactory = new ConsumerRecordFactory[String, String](
      "data-input-topic",
      new StringSerializer(),
      new StringSerializer()
    )

    val configFactory = new ConsumerRecordFactory[String, String](
      "config-input-topic",
      new StringSerializer(),
      new StringSerializer()
    )

    val configTemplate =
      (l: List[String], k: String) =>
        s"""{"alist":${l.mkString("[\"", "\",\"", "\"]")},"akey":\"$k\"}"""

    val configs = List(
      configFactory.create(
        "config-input-topic",
        "collection1#mapping",
        configTemplate(List("a", "b", "c"), "avalue")
      ),
      configFactory.create(
        "config-input-topic",
        "collection2#mapping",
        configTemplate(List("1", "2", "3"), "bvalue")
      ),
      configFactory.create(
        "config-input-topic",
        "collection1#transform",
        configTemplate(List("x", "y"), "cvalue")
      )
    ).asJava

    testDriver.pipeInput(configs)

    val setRecordIdHeader = (value: String) =>
      new RecordHeaders().add(new RecordHeader("recordSetId", value.getBytes))

    val data = List(
      dataFactory.create(
        "data-input-topic",
        "key3",
        "value3",
        setRecordIdHeader("collection3")
      ),
      dataFactory.create(
        "data-input-topic",
        "key2",
        "value2",
        setRecordIdHeader("collection2")
      ),
      dataFactory.create(
        "data-input-topic",
        "key1",
        "value1",
        setRecordIdHeader("collection1")
      )
    ).asJava
    testDriver.pipeInput(data)

    val output = testDriver.readOutput(
      "output-topic",
      new StringDeserializer,
      new StringDeserializer
    )

    try {
      OutputVerifier.compareKeyValue(
        output,
        "key2",
        "value2#bvalue"
      )

      OutputVerifier.compareKeyValue(
        testDriver.readOutput(
          "output-topic",
          new StringDeserializer,
          new StringDeserializer
        ),
        "key1",
        "value1#avalue"
      )
    } finally {
      testDriver.close()
    }
  }
}

case class TestConfig(alist: List[String], akey: String)

object TestConfigHelper {
  val parse: Array[Byte] => TestConfig = (input: Array[Byte]) =>
    read[TestConfig](input)
}

object TestConfig {
  implicit val rw: RW[TestConfig] = macroRW
}

class TestConfigSerDe extends Serde[TestConfig] {
  override def serializer(): Serializer[TestConfig] = new TestConfigSerializer

  override def deserializer(): Deserializer[TestConfig] =
    new TestConfigDeserializer
}

class TestConfigSerializer extends Serializer[TestConfig] {
  override def serialize(topic: String, data: TestConfig): Array[Byte] = {
    write(data).getBytes
  }
}

class TestConfigDeserializer extends Deserializer[TestConfig] {
  override def deserialize(topic: String, data: Array[Byte]): TestConfig = {
    TestConfigHelper.parse(data)
  }
}
