/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase

import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.kstream.{Transformer, TransformerSupplier}
import org.apache.kafka.streams.processor.ProcessorContext

private class RecordIdSupplier[T]
    extends TransformerSupplier[String, T, KeyValue[String, KeyedValue[T]]] {
  override def get()
      : Transformer[String, T, KeyValue[String, KeyedValue[T]]] =
    new Transformer[String, T, KeyValue[String, KeyedValue[T]]] {
      var context: ProcessorContext = _
      override def init(context: ProcessorContext): Unit =
        this.context = context

      override def transform(
          key: String,
          value: T
      ): KeyValue[String, KeyedValue[T]] = {

        val recordSetId = context
          .headers()
          .headers("recordSetId")
          .iterator()
          .next()
          .value()
          .map(_.toChar)
          .mkString
        new KeyValue[String, KeyedValue[T]](recordSetId, KeyedValue(key, value))
      }

      override def close(): Unit = {}
    }
}
