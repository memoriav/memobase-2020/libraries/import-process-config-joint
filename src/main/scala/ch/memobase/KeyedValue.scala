/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, ObjectInputStream, ObjectOutputStream}
import java.util

import org.apache.kafka.common.serialization.{Deserializer, Serde, Serializer}

private case class KeyedValue[T](key: String, value: T)

private class KeyedValueSerDe[T] extends Serde[KeyedValue[T]] {
  override def configure(configs: util.Map[String, _], isKey: Boolean) {}

  override def close() {}

  override def deserializer(): Deserializer[KeyedValue[T]] =
    new KeyedValueDeserializer[T]

  override def serializer(): Serializer[KeyedValue[T]] =
    new KeyedValueSerializer[T]
}

private class KeyedValueSerializer[T] extends Serializer[KeyedValue[T]] {
  override def configure(
                          configs: util.Map[String, _],
                          isKey: Boolean
                        ): Unit = {}

  override def serialize(topic: String, data: KeyedValue[T]): Array[Byte] = {
    try {
      val byteOut = new ByteArrayOutputStream()
      val objOut = new ObjectOutputStream(byteOut)
      objOut.writeObject(data)
      objOut.close()
      byteOut.close()
      byteOut.toByteArray
    } catch {
      case ex: Exception => throw new Exception(ex.getMessage)
    }
  }

  override def close(): Unit = {}
}

private class KeyedValueDeserializer[T] extends Deserializer[KeyedValue[T]] {
  override def configure(
                          configs: util.Map[String, _],
                          isKey: Boolean
                        ): Unit = {}

  override def deserialize(topic: String, data: Array[Byte]): KeyedValue[T] = {
    val byteIn = new ByteArrayInputStream(data)
    val objIn = new ObjectInputStream(byteIn)
    val obj = objIn.readObject().asInstanceOf[KeyedValue[T]]
    byteIn.close()
    objIn.close()
    obj
  }

  override def close(): Unit = {}
}
