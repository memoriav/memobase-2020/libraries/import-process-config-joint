/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase

import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.kstream.{KStream, KTable, ValueJoiner}
import org.apache.logging.log4j.scala.Logging

/**
 * @param service       Service type (i.e. name of step in import process)
 * @param dataSerDe     SerDe instance used to (de-)serialise the data records
 * @param configSerDe   SerDe instance used to (de-)serialise config objects
 * @param parseFunction Function used to parse raw config data into config object
 * @tparam T Type of incoming records
 * @tparam U Target type of config objects
 */
class ConfigJoiner[T, U](
    service: String,
    implicit val dataSerDe: Serde[T],
    implicit val configSerDe: Serde[U],
    parseFunction: Array[Byte] => U
) extends Logging {

  private def getCollectionName(key: String) = key.split("#")(0)

  /**
    * Joins data records with collection-specific configuration
    * @param dataStream KStream of data records
    * @param configStream KStream with raw config objects
    * @return Records joined with respective configuration
    */
  def join(
      dataStream: KStream[String, T],
      configStream: KStream[String, Array[Byte]]
  ): KStream[String, (T, U)] = {

    implicit val keyedValueSerDe: Serde[KeyedValue[T]] = new KeyedValueSerDe

    val configTable = configStream
      .filter((k, _) => k.split("#")(1) == service)
      .map[String, U]((key: String, value: Array[Byte]) =>
        new KeyValue(getCollectionName(key), parseFunction(value))
      )
      .groupByKey
      .reduce((_, newVal) => newVal)

    //noinspection ConvertExpressionToSAM
    dataStream
      .transform(new RecordIdSupplier[T])
      .join[U, (KeyedValue[T], U)](
        configTable: KTable[String, U],
        new ValueJoiner[KeyedValue[T], U, (KeyedValue[T], U)] {
          override def apply(value1: KeyedValue[T], value2: U)
          : (KeyedValue[T], U) = (value1, value2)
        }
      )
      .map[String, (T, U)]((_, oldVal) =>
        new KeyValue(oldVal._1.key, (oldVal._1.value, oldVal._2))
      )
  }
}

object ConfigJoiner {

  /**
    * @param service Service type (i.e. name of step in import process)
    * @param dataSerDe SerDe instance used to (de-)serialise the data records
    * @param configSerDe SerDe instance used to (de-)serialise config objects
    * @param parseFunction Function used to parse raw config data into config object
    * @tparam T Type of incoming records
    * @tparam U Target type of config objects
    * @return Instance of ConfigJoiner class
    */
  def apply[T, U](
      service: String,
      dataSerDe: Serde[T],
      configSerDe: Serde[U],
      parseFunction: Array[Byte] => U
  ): ConfigJoiner[T, U] =
    new ConfigJoiner(service, dataSerDe, configSerDe, parseFunction)
}
