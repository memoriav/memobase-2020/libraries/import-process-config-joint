import sbt._

object Dependencies {
  lazy val kafkaV = "2.3.1"
  lazy val log4jV = "2.11.2"

  lazy val kafkaStreams = "org.apache.kafka" %% "kafka-streams-scala" % kafkaV
  lazy val kafkaStreamsTestUtils = "org.apache.kafka" % "kafka-streams-test-utils" % kafkaV
  lazy val log4jApi = "org.apache.logging.log4j" % "log4j-api" % log4jV
  lazy val log4jScala = "org.apache.logging.log4j" %% "log4j-api-scala" % "11.0"
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.1.1"
  lazy val uPickle = "com.lihaoyi" %% "upickle" % "0.9.5"
}
